package tk.maxmeijer.equationsolver;

public class TooManyClosedBracketsException extends BracketsException{
	public String getMessage() {
		return "There were too many closed brackets";
	}
}
