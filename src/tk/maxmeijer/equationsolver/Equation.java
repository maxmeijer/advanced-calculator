package tk.maxmeijer.equationsolver;

public class Equation{
	Expression[] sidesOfEquation;

	public Equation(String equation) throws EquationParseException{
		checkForValidity(equation);
		initSidesOfEquation(equation);

	}

	private void initSidesOfEquation(String equation) throws EquationParseException{
		String[] split = equation.split("=");
		sidesOfEquation = new Expression[split.length];
		for (int i = 0; i < split.length; i++) {
			sidesOfEquation[i] = new Expression(split[i]);
		}
	}

	private void checkForValidity(String equation)
			throws EquationParseException{
		if (!equation.matches(".+=.+"))
			throw new EquationParseException();
	}

	String solveOneStep(String equation){
		return equation;
	}

	void combineLikeTerms(){

	}

	public String toString(){
		String format = "";
		for (int i = 0; i < sidesOfEquation.length; i++) {
			if (i != 0)
				format = format.concat("=");
			format = format.concat(sidesOfEquation[i].toString());
		}
		return format;
	}
}
