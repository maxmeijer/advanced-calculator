package tk.maxmeijer.equationsolver;

public class NoPowerSpecifiedException extends EquationParseException{
	public String getMessage() {
		return "There was a power expected following the ^, but it wasn't there";
	}
}
