package tk.maxmeijer.equationsolver;

public class Fraction{
	int numerator;
	int denominator=1;
	
	public Fraction(int numerator,int denominator){
		this.numerator=numerator;
		if(denominator==0)throw new DivideByZeroException();
		this.denominator=denominator;
	}
	
	public Fraction(String parseString) {
		String[] array=parseString.split("/");
		numerator=new Integer(array[0]);
		if(array.length>=2)denominator=new Integer(array[1]);
	}
	
	public Fraction(int integer) {
		this(integer,1);
	}
	
	public Fraction(Fraction frac){
		numerator=frac.numerator;
		denominator=frac.denominator;
	}

	public void multiplyBy(int integer) {
		multiplyBy(new Fraction(integer));
	}
	
	public void multiplyBy(Fraction fraction) {
		denominator *= fraction.denominator;
		numerator *= fraction.numerator;
		simplify();
	}
	
	public void divideBy(int integer) {
		divideBy(new Fraction(integer));
	}
	
	public void divideBy(Fraction fraction) {
		multiplyBy(fraction.getReciprocal());
	}
	
	public Fraction getReciprocal() {
		return new Fraction(denominator,numerator);
	}
	
	public void simplify() {
		int GCD=MathUtils.getGCD(numerator, denominator);
		numerator=numerator/GCD;
		denominator=denominator/GCD;
	}
	
	public String toString() {
		StringBuilder s=new StringBuilder();
		s.append(numerator);
		if(denominator!=1) {
			s.append('/');
			s.append(denominator);
		}
		return s.toString();
	}
	
	
	
	@Override
	public boolean equals(Object o) {
		simplify();
		try {
			Fraction f=(Fraction)o;
			f.simplify();
			return f.denominator==denominator&&f.numerator==numerator;
		}catch(ClassCastException e) {
			try {
				Integer i=(Integer)o;
				return denominator==1&&numerator==i;
			}catch(NumberFormatException ex) {
				return false;
			}
		}
	}

	public Fraction add(Fraction constant){
		Fraction result=new Fraction(this);
		int LCM=MathUtils.getLCM(denominator, constant.denominator);
		result.denominator=LCM;
		result.numerator=constant.numerator*LCM/constant.denominator+
					numerator*LCM/denominator;
		return result;
	}
}