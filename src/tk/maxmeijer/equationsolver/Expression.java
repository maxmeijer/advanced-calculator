package tk.maxmeijer.equationsolver;

import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.transform.Result;

public class Expression{
	ArrayList<Term> terms;

	public Expression(String expression) throws EquationParseException{

		initTerms(expression);
	}

	public Expression(Term term){
		terms=new ArrayList<Term>();
		terms.add(term);
	}

	public Expression(Expression expression){
		expression.terms=(ArrayList<Term>) terms.clone();
	}

	private void initTerms(String expression) throws EquationParseException{
		String[] split = splitTerms(expression);
		terms = new ArrayList<Term>();
		for (String term : split) {
			terms.add(parseTerm(term));
		}
	}

	private String[] splitTerms(String expression){
		expression = modifySigns(expression);
		String[] split = expression.split("\\+");
		return split;
	}
	
	public void addBy(Term term) {
		terms.add(term);
	}
	
	public void addBy(Expression expr) {
		for(Term term : expr.terms) {
			addBy(term);
		}
	}
	
	public void multiplyBy(Expression expr) {
		ArrayList<Term> result=(ArrayList<Term>) terms.clone();
		int j=0;
		for (int i = 0; i < terms.size(); i++,j++) {// loop through the original while pushing the changes to result
			for (Term term : expr.terms) {
				if(!term.isComplex()) {
					result.add(terms.get(i).multiply((SimpleTerm)term));
				}else {
					result.add(terms.get(i).multiply((ComplexTerm)term));
				}
			}
			if(expr.terms.size()>0)result.remove(j--);
		}
		terms=result;
	}
	
	private Term parseTerm(String term) throws EquationParseException{
		if(!isComplexTerm(term)) {
			return new SimpleTerm(term);
		}else {
			return new ComplexTerm(term);
		}
	}

	private boolean isComplexTerm(String term){
		String regex = ".*[()].*";
		return term.matches(regex);
	}

	/**
	 * Places extra + signs in front of - signs and changes -- into +
	 * 
	 * @param expression
	 *            The expression to be modified
	 * @return The modified expression
	 */
	private String modifySigns(String expression){
		char[] array = expression.toCharArray();
		int extraAdded = 0;
		for (int i = 1; i < array.length; i++) {
			if (array[i] == '-') {
				if (array[i + 1] == '-') {
					// change -- into +
					expression = expression.substring(0, i + extraAdded)
							.concat("+")
							.concat(expression.substring(i + 2 + extraAdded));
					extraAdded--;
				} else if (!isOperator(array[i - 1])) {
					// change for example 3-2 into 3+-2
					expression = expression.substring(0, i + extraAdded)
							.concat("+")
							.concat(expression.substring(i + extraAdded));
					extraAdded++;
				}
			}
		}
		return expression;
	}

	private boolean isOperator(char c){
		switch (c) {
			case '+':
			case '-':
			case '*':
			case '/':
				return true;
			default:
				return false;
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Term t:terms) {
			sb.append(t.toString());
			sb.append("+");
		}sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public boolean equals(Object o) {
		try {
			Expression expr=(Expression) o;
			//TODO better checking
			return expr.toString().equals(toString());
		}catch(ClassCastException e) {
			return false;
		}
	}
	
	public void convertComplexToSimpleTermsWherePossible() {
		for (int i = 0; i < terms.size(); i++) {
			terms.set(i, terms.get(i).convertToSimpleTermIfPossible());
		}
	}
	
	public void combineLikeTerms() {
		int size=terms.size();
		@SuppressWarnings("unchecked")
		ArrayList<Term> result=(ArrayList<Term>) terms.clone();
		for (int i = 0; i < size; i++) {
			Term term1=terms.get(i);
			for(int j=i+1,k=j;j<size;j++,k++) {
				Term term2=terms.get(j);
				if(term1.isLikeTerm(term2)) {
					terms.set(k,term1.combine(term2));
				}
				result.remove(k--);
			}
		}
	}
	
	//work in progress...
	public boolean isAlike(Expression otherExpression) {
		Fraction ratio=new Fraction(0);
		if(terms.size()!=otherExpression.terms.size())return false;
		for (Term thisTerm : terms) {
			try {
				Term likeTerm=otherExpression.findLikeTerm(thisTerm);
				if(likeTerm==null)return false;
				Fraction ratioBetweenThisTermAndLikeTerm=thisTerm.getRatio(likeTerm);
				if(ratio.equals(0))ratio=ratioBetweenThisTermAndLikeTerm;
				if(ratio!=ratioBetweenThisTermAndLikeTerm)return false;
			}catch(DividingByExpressionsNotSupportedException e) {
				throw new CombiningLikeTermsNotSupportedForNestedBracketsException();
			}
		}return true;
	}

	private Term findLikeTerm(Term termToFind){
		for(Term thisTerm : terms) {
			if(thisTerm.isLikeTerm(termToFind)) {
				return thisTerm;
			}
		}
		return null;
	}

	protected void combineWith(Term term){
		if(!term.isComplex()) {
			terms.set(0, terms.get(0).combine(term));
		}else {
			multiplyBy(new Expression("2"));
		}
	}
}
