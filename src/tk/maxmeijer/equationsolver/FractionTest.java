package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FractionTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testMultiplyByFraction(){
		Fraction f=new Fraction(2,5);
		f.multiplyBy(new Fraction(2,4));
		Fraction expected=new Fraction(4,20);
		assertEquals(expected,f);
	}

	@Test
	public void testDivideByFraction(){
		Fraction f=new Fraction(2,5);
		f.divideBy(new Fraction(4,2));
		Fraction expected=new Fraction(4,20);
		assertEquals(expected,f);
	}

	@Test
	public void testGetReciprocal(){
		Fraction f=new Fraction(2,5);
		Fraction result=f.getReciprocal();
		Fraction expected=new Fraction(5,2);
		assertEquals(expected,result);
	}

	@Test
	public void testSimplify(){
		Fraction f=new Fraction(10,2);
		Fraction expected=new Fraction(5,1);
		assertEquals(expected,f);
	}

	@Test
	public void testToString(){
		Fraction f=new Fraction(10,2);
		assertEquals("10/2",f.toString());
	}

}
