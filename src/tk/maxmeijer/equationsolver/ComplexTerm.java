package tk.maxmeijer.equationsolver;

import java.util.ArrayList;

public class ComplexTerm implements Term{
	Expression expression;

	public ComplexTerm(String term) throws EquationParseException{
		initFactors(term);
	}
	
	private ComplexTerm(ComplexTerm t) {
		expression=new Expression(t.expression);
	}
	
	private void initFactors(String term) throws EquationParseException{
		String[] f = splitTerm(term);
		expression = new Expression("1");
		for (int i = 0; i < f.length; i++) {
			multiplyBy(new Expression(f[i]));
		}
	}

	/**
	 * splits a term with brackets into separate strings that are the different
	 * factors and can be passed to the Expression constructor. If the string
	 * contains less closing brackets than necessary the function will act as if
	 * the closing bracket is at the end of the string
	 * 
	 * @param term
	 *            the term that needs to be split
	 * @return an array with all the strings without brackets around them
	 * @throws TooManyClosedBracketsException
	 *             if the string contains a closed bracket that doesn't match up
	 *             with any previous opening bracket
	 */
	static String[] splitTerm(String term) throws TooManyClosedBracketsException{

		ArrayList<String> result = new ArrayList<String>();
		int openedBrackets = 0, started = 0;
		char[] array = term.toCharArray();
		for (int i = 0; i < array.length; i++) {

			if (array[i] == ')') {
				openedBrackets--;
				if (openedBrackets < 0) throw new TooManyClosedBracketsException();
			}

			if (openedBrackets == 0) 
			if (array[i] == '(' || array[i] == '*' || array[i] == ')') {
				int sequenceLength = i - started;
				result = addIfNotEmpty(result, array, started, sequenceLength);
				started = i + 1;
			}

			if (array[i] == '(') openedBrackets++;
		}
		int sequenceLength = array.length - started;
		result = addIfNotEmpty(result, array, started, sequenceLength);
		return result.toArray(new String[result.size()]);
	}

	private static ArrayList<String> addIfNotEmpty(ArrayList<String> list, char[] array, int start, int length){
		if (length > 0) {
			list.add(new String(array, start, length));
		}
		return list;
	}

	public String toString(){
		String result = "";
		result = result.concat("(").concat(expression.toString()).concat(")");
		return result;
	}

	@Override
	public void multiplyBy(String factor){
		multiplyBy(new Expression(factor));
	}

	@Override
	public Term multiply(SimpleTerm term){
		ComplexTerm c=new ComplexTerm(this);
		c.multiplyBy(new Expression(term));
		return c;
	}

	private void multiplyBy(Expression expr){
		expression.multiplyBy(expr);
	}

	@Override
	public Term multiply(ComplexTerm term){
		ComplexTerm c=new ComplexTerm(this);
		c.multiplyBy(new Expression(term));
		return c;
	}

	@Override
	public boolean isComplex(){
		return true;
	}

	@Override
	public boolean isLikeTerm(Term t){
		return expression.isAlike(new Expression(t));
	}
	public void combineWith(Term term){
		expression.combineLikeTerms();
		if(term.isComplex()) {
			ComplexTerm c=(ComplexTerm) term;
			c.expression.combineLikeTerms();
			term=c;
		}
		expression.combineWith(term);
	}

	@Override
	public Fraction getRatio(Term likeTerm){
		throw new DividingByExpressionsNotSupportedException();
	}

	@Override
	public Term convertToSimpleTermIfPossible(){
		expression.combineLikeTerms();
		if(expression.terms.size()==1) {
			return expression.terms.get(0);
		}
		return this;
	}

	@Override
	public Term combine(Term term){
		ComplexTerm result=new ComplexTerm(this);
		result.combineWith(term);
		return result;
	}
}
