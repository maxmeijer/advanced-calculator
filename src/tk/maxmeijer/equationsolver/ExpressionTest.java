package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExpressionTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testExpression(){
		Expression expr=new Expression("(3x*2)(5x)+5y");
		assertEquals(new Expression("(6x)(5x)+5y"),expr);
	}

	/*@Test
	public void testModifySigns(){
		Expression exp=new Expression("3x");
		assertEquals("3+-2+-2+3+-5+3", exp.modifySigns("3-2+-2+3-5--3"));
	}*/

}
