package tk.maxmeijer.equationsolver;

/**
 * 
 * @author max
 * @version 0.0.2
 * @since 0.0.0
 * 
 *        for a parse string there are a few rules:
 * 
 *        regex:^\-?(?:\d+|[a-zA-Z])(?:\^\(\-?\d+\))?(?:[a-zA-Z](?:\^\(\-?\d+\))
 *        ?)*(?:(?:\/|\*)(?:(?:(?:​\-?\d+)|(?:\d+|(?:[a-zA-Z](?:\^\(\-?\d+\))?))
 *        )(?:[a-zA-Z](?:\^\(\-?\d+\))?)*))*(?:[a-zA-Z](?:\^\(\-​?\d+\)?)?)?$
 * 
 *        about the ^: the parse string can not start with it it should always
 *        be followed by () there should be a number inside the parentheses
 *        expressions inside the parentheses are NOT allowed it can not be used
 *        on a number it can not be used after a /, *, ^, (, or )
 * 
 *        about the (: can only be used after a ^ it is possible that it is not
 *        closed, there will be added a ) at the end of the whole string
 * 
 *        about the ): it must always match a previous ( if it doesn't an
 *        EquationParseException (TooManyClosedBracketsException) will be thrown
 * 
 *        about the *: the parse string cannot start with it it cannot be used
 *        after a /, *, ^, or (
 * 
 *        about the /: the parse string cannot start with it it cannot be used
 *        after a /, *, ^, or (
 * 
 *        about the -: the parse string can start with it it can't be used as an
 *        operator it can only be used as a negative sign so like the (-) button
 *        on a calculator
 * 
 *        docs created: 3-1-2014 docs last modified: 3-1-2014
 */
public class SimpleTerm implements Term{
	Variable v = new Variable("1");

	public SimpleTerm(String variable) throws EquationParseException{
		variable = modifyFractions(variable);
		addFactors(splitFactors(variable));
	}

	private SimpleTerm(Variable v){
		this.v=v;
	}

	public SimpleTerm(SimpleTerm simpleTerm){
		this.v=new Variable(simpleTerm.v);
	}

	private static String modifyFractions(String variable){
		return variable.replaceAll("/", "*1/");
	}

	private void addFactors(String[] factors) throws EquationParseException{
		for (int i = 0; i < factors.length; i++)
			addFractions(splitFractions(factors[i]));
	}

	private void addFractions(String[] fraction){
		multiplyBy(fraction[0]);
		for (int i = 1; i < fraction.length; i++)
			divideBy(fraction[i]);
	}

	public void divideBy(String divisor){
		v.divideBy(new Variable(divisor));
	}

	private static String[] splitFractions(String fractions){
		return fractions.split("/");
	}

	private static String[] splitFactors(String factors){
		return factors.split("\\*");
	}

	public void multiplyBy(String factor) throws EquationParseException{
		v.multiplyBy(new Variable(factor));
	}

	public String toString(){
		return v.toString();
	}

	public boolean equals(Object other) {
		SimpleTerm o = (SimpleTerm) other;
		return v.equals(o.v);
	}
	
	public Term multiply(SimpleTerm term) {
		Variable result=new Variable("1");
		result.multiplyBy(v);
		result.multiplyBy(term.v);
		return new SimpleTerm(result);
	}
	
	public Term multiply(ComplexTerm term) {
		return term.multiply(this);
	}
	
	public boolean isLikeTerm(Term term) {
		if(term.isComplex())return term.isLikeTerm(this);
		SimpleTerm t=(SimpleTerm) term;
		return v.isLikeTerm(t.v);
	}

	@Override
	public boolean isComplex(){
		return false;
	}

	@Override
	public Term combine(Term term){
		if(term.isComplex())return term.combine(this);
		SimpleTerm t=(SimpleTerm) term;
		SimpleTerm result=new SimpleTerm(this);
		result.v.combineWith(t.v);
		return result;
	}

	@Override
	public Fraction getRatio(Term likeTerm){
		try {
			SimpleTerm simpleLikeTerm=(SimpleTerm) likeTerm;
			Fraction result=v.getConstant();
			result.divideBy(simpleLikeTerm.v.getConstant());
			return result;
		}catch(ClassCastException e) {
			return likeTerm.getRatio(this).getReciprocal();
		}
	}

	@Override
	public Term convertToSimpleTermIfPossible(){
		return this;
	}
}
