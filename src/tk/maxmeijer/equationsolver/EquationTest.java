package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EquationTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testSolveOneStep(){
	}

	@Test
	public void testCombineLikeTerms(){
		Equation eq;
		try {
			eq=new Equation("2x+5+3x=7ab-9a+11b+5ab");
		} catch (EquationParseException e) {
			fail("parse exception thrown");
			return;
		}
		eq.combineLikeTerms();
		assertEquals(eq.toString(),"5x+5=12ab-9a+11b");
	}
	
	@Test
	public void testToString() {
		Equation eq;
		try {
			eq=new Equation("2x+5+3x=7ab-9a+11b+5ab");
		} catch (EquationParseException e) {
			fail("parse exception thrown");
			return;
		}
		assertEquals(eq.toString(),"2x+5+3x=7ab-9a+11b+5ab");
	}
}
