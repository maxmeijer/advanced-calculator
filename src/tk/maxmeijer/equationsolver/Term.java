package tk.maxmeijer.equationsolver;

public interface Term{
	public String toString();
	public boolean equals(Object o);
	public void multiplyBy(String factor);
	public Term multiply(SimpleTerm term);
	public Term multiply(ComplexTerm term);
	public boolean isComplex();
	public boolean isLikeTerm(Term t);
	public Term combine(Term term);
	public Fraction getRatio(Term likeTerm);
	public Term convertToSimpleTermIfPossible();
}
