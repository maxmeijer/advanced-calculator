package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VariableTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testVariable(){
	}
	
	@Test
	public void testMultiplyBy() throws EquationParseException {
		Variable v1=new Variable("-5ab");
		Variable v2=new Variable("-5a");
		v1.multiplyBy(v2);
		assertEquals("25a^(2)b",v1.toString());
	}
	
	@Test
	public void testToString() {
	}

}
