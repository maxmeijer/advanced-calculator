package tk.maxmeijer.equationsolver;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Variable{
	private Fraction constant = new Fraction(1);
	private LinkedHashMap<String, Integer> variables = new LinkedHashMap<String, Integer>();
	private String parseString;
	public static final Variable ONE=new Variable("1");

	public Variable(String variable) throws EquationParseException{
		parseString = variable;
		parse();
	}

	public Variable(Variable v){
		this.constant=new Fraction(v.constant);
		this.variables=(LinkedHashMap<String, Integer>) v.variables.clone();
	}

	private void parseVariables(String parseString) throws EquationParseException{
		int length = parseString.length();
		for (int i = 0; i < length; i++) {
			int power = 1;
			String thisVariable=parseString.substring(i, i + 1);
			try {
				if (parseString.charAt(i+1) == '^') {
					i+=2;
					try {
						int start=i+1;
						i=getBracketsEnd(parseString,i);
						power=Integer.parseInt(parseString.substring(start,i));
					} catch (StringIndexOutOfBoundsException e) {
						throw new NoPowerSpecifiedException();
					}catch(NumberFormatException e) {
						throw new NoVariableInPowerAllowedException();
					}
				}
			} catch (StringIndexOutOfBoundsException e) {
			}
			multiplyBySingleVariable(thisVariable, power);
		}
	}

	private int getBracketsEnd(String string,int start) throws EquationParseException{
		char[] array = string.toCharArray();
		if (array[start++] != '(') throw new ParenthesesExpectedException();
		int i;
		for (i = start; i < array.length; i++) {
			if (array[i] == ')') break;
		}
		return i;

	}

	private void parse() throws EquationParseException{
		int index = getVariablesStartIndex();
		String constantPart = parseString.substring(0, index);
		String variablePart = parseString.substring(index);
		parseConstant(constantPart);
		parseVariables(variablePart);
	}

	private void parseConstant(String constantPart){
		int length=getDigitLength(constantPart);
		if(length==0)return;
		int power=1;
		int thisConstant;
		try {
			thisConstant = new Integer(constantPart.substring(0,length));
			if(length<constantPart.length()-3&&constantPart.charAt(length++)=='^') {
				int bracketsEnd=getBracketsEnd(constantPart, length);
				power=Integer.parseInt(constantPart.substring(length+1,bracketsEnd));
			}
		}catch(NumberFormatException e) {
			if(constantPart.length()==1 && constantPart.charAt(0)=='-')thisConstant=-1;
			else throw new NegativeUsedToSubtractException();
		}
		
		constant=MathUtils.power(thisConstant,power);
	}

	private int getDigitLength(String constantPart){
		char[] array = parseString.toCharArray();
		int i;
		for (i = 0; i < array.length && isDigit(array[i]); i++);
		return i;
	}

	public void multiplyBy(Variable v){
		constant.multiplyBy(v.constant);
		for (Entry<String, Integer> variable : v.variables.entrySet()) {
			multiplyBySingleVariable(variable.getKey(), variable.getValue());
		}
	}
	
	public void divideBy(int number) {
		constant.divideBy(number);
	}

	public void toThePowerOf(int power){
		if (power < 0) throw new IllegalArgumentException("no negative powers allowed (yet)");
		for (int i = 0; i < power; i++) {
			multiplyBy(this);
		}
	}

	private void multiplyBySingleVariable(String name, int power){
		if (variables.containsKey(name)) power += variables.get(name);
		variables.put(name, power);
	}

	private int getVariablesStartIndex(){
		char[] array = parseString.toCharArray();
		int i;
		for (i = 0; i < array.length && isConstant(array[i]); i++);
		return i;
	}

	private static boolean isConstant(char c){
		return isDigit(c) || c=='^' || c=='(' || c==')';
	}
	
	private static boolean isDigit(char c) {
		return c >= '0' && c <= '9' || c == '-';
	}
	
	public boolean hasVariables() {
		return variables.size()>0;
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder();
		if (!constant.equals(1)) {
			if(!constant.equals(-1) || !hasVariables())result.append(constant);
			else if(constant.equals(-1))result.append("-");
		}
		if (!constant.equals(0)) result.append(stringifyVariables());
		return result.toString();
	}

	private String stringifyVariables(){
		StringBuilder result = new StringBuilder();
		for (Entry<String, Integer> variable : variables.entrySet()) {
			result.append(stringifyVariableEntry(variable));
		}
		return result.toString();
	}

	private String stringifyVariableEntry(Entry<String, Integer> variable){
		int power = variable.getValue();
		StringBuilder result = new StringBuilder();
		if(power < 0) {
			variable.setValue(-variable.getValue());
			result.append("/(").append(stringifyVariableEntry(variable)).append(")");
		}
		else if (power > 0) {
			result.append(variable.getKey());
			if (power != 1) result.append("^(").append(power).append(")");
		}
		return result.toString();
	}
	
	public boolean equals(Variable other) {
		return other.constant.equals(constant)&&variables.equals(other.variables);
	}

	public void divideBy(Variable v){
		constant.divideBy(v.constant);
		for (Entry<String, Integer> variable : v.variables.entrySet()) {
			divideBySingleVariable(variable.getKey(), variable.getValue());
		}
	}

	private void divideBySingleVariable(String name, Integer power){
		if (variables.containsKey(name)) power += variables.get(name);
		variables.put(name, -power);
	}

	public boolean isLikeTerm(Variable v){
		return variables.equals(v.variables);
	}

	public void combineWith(Variable v){
		constant=constant.add(v.constant);
	}
	
	protected Fraction getConstant() {
		return constant;
	}
}
