package tk.maxmeijer.equationsolver;

public class MathUtils{
	//Euclidean algorithm
	static int getGCD(int a, int b) {
		while(b!=0) {
			int temp=b;
			b=a%b;
			a=temp;
		}return a;
	}

	static int getLCM(int a, int b)
	{
	    return a * (b / getGCD(a, b));
	}

	
	public static Fraction power(int base, int power){
		if(power<0) {
			return power(base,-power).getReciprocal();
		}else if(power==0) {
			return new Fraction(1);
		}else {
			return new Fraction((int)Math.pow(base, power));
		}
	}
}
