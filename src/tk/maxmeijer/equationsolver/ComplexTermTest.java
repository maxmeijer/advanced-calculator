package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ComplexTermTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testComplexTerm() throws EquationParseException{
		ComplexTerm complexTerm=new ComplexTerm("3x(5x+4)53/abc");
		assertEquals("(795x^(2)/(a)/(b)/(c)+636x/(a)/(b)/(c))",complexTerm.toString());
		//fail("Not yet implemented");
	}

	

	@Test
	public void testToString(){
		//fail("Not yet implemented");
	}
	
	@Test
	public void testSplitTerm() {
		try {
			assertArrayEquals(new String[] {
					"3x",
					"5x*53+3y",
					"2",
					"43+6r",
					"5x"
			},ComplexTerm.splitTerm("3x(5x*53+3y)*2(43+6r)5x"));
		} catch (TooManyClosedBracketsException e) {
			e.printStackTrace();
		}
	}

}
