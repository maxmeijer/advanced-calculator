package tk.maxmeijer.equationsolver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SimpleTermTest{

	@Before
	public void setUp() throws Exception{
	}

	@After
	public void tearDown() throws Exception{
	}

	@Test
	public void testSimpleTerm() throws EquationParseException{
		SimpleTerm result=new SimpleTerm("3x^(2)*5/3*y");
		SimpleTerm expected=new SimpleTerm("5x^(2)y");
		assertEquals(expected,result);
	}

	@Test
	public void testMultiplyBy() throws EquationParseException{
		SimpleTerm result=new SimpleTerm("15xy");
		result.multiplyBy("2x");
		SimpleTerm expected=new SimpleTerm("30x^(2)y");
		assertEquals(expected,result);
	}

	@Test
	public void testToString() throws EquationParseException{
		String result=new SimpleTerm("3x/ay").toString();
		String expected="3x/(a)/(y)";
		assertEquals(expected,result);
	}

}
